<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr"> 
   <head> 
      <title>Blog</title> 
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
      <link href="articles.css" rel="stylesheet">
<body> 

   <?php 
   $connect = mysqli_connect("localhost", "root", "root", "blogPHP"); 
 
   /* Vérification de la connexion */ 
   if (!$connect) { 
      echo "Échec de la connexion : ".mysqli_connect_error(); 
      exit(); 
   } 
 
   $requete = "SELECT * FROM Article  "; 
   if ($resultat = mysqli_query($connect,$requete)) { 
      date_default_timezone_set('Europe/Paris'); 
      /* fetch le tableau associatif */ 
      while ($ligne = mysqli_fetch_assoc($resultat)) { 
         $dt_debut = date_create_from_format('Y-m-d H:i:s', $ligne['Date']); 
  
         echo "<div class='galerie'>";
         echo "<div class=''>";
         echo '<h2><a title="'.$ligne['Titre'].$ligne['Description'].'">'.$ligne['Titre'].'</a></h2>';
         echo "<img  src='photos/".$ligne['Photo']."'width=200 height=150/>";
         echo "<br> <br>";
         echo "</div>";
         echo "</div>";
      } 
   } 
   ?> 
   <br /> 

   <a href="formulaireAjout.php">Ajouter une oeuvre</a> 
</body> 
</html>