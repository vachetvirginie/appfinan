<?php

// Initialize the session

session_start();



?>
<!DOCTYPE html>
<html class="gr__thewildhoneypie_com" data-scrapbook-source="http://www.thewildhoneypie.com/" data-scrapbook-create="20180110104724337" lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="P22.studio">

    <link rel="apple-touch-icon" sizes="180x180" href="http://www.thewildhoneypie.com/apple-touch-icon.d19bc23240a3c289080ce9f51fb54f99.png">
    <meta name="apple-mobile-web-app-title" content="TWHP">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-16x16.997c5bbe27756f6cdf5d8909a713e6c2.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.997c5bbe27756f6cdf5d8909a713e6c2.png">
    <link rel="manifest" href="http://www.thewildhoneypie.com/favicons/manifest.61d6fd10a9add39d36ac95340fea56d2.json">
    <link rel="mask-icon" href="http://www.thewildhoneypie.com/safari-pinned-tab.d0781f684347d583392d21df8ddf604b.svg" color="#78c6ff">
    <meta name="theme-color" content="#ffffff">

    <title>Where are my graffs</title>

    <meta property="og:title" content="The Wild Honey Pie">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="The Wild Honey Pie">
    <meta property="og:description" content="The Wild Honey Pie is a music discovery community where you can submit songs and find your next favorite bands">
    <meta name="description" content="The Wild Honey Pie is a music discovery community where you can submit songs and find your next favorite bands">
    <meta property="og:image" content="http://www.thewildhoneypie.com/og-image.png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:url" content="http://www.thewildhoneypie.com/">

    <link href="styles.css" rel="stylesheet">

    <script async=""></script>
    <script async=""></script>
</head>

<body data-gr-c-s-loaded="true" cz-shortcut-listen="true">
    <div id="content">
        <div class="header">
            <!-- <a class="header__logo" href="">
                <img alt="The Wild Honey Pie Logo" srcset="d9a6481f70a524b08612aaf4643af892-360.png 360w,
                  b22f226f3161cb61192a3eb317fc271b-500.png 500w,
                  c7c2c3c5702dd6d34b037edd18d8de32-720.png 720w,
                  f5aa6fa00929329a8c0025501621231a-870.png 1000w" sizes="(max-width: 768px) 86vw, (max-width: 992px) 500px, 360px" src="b22f226f3161cb61192a3eb317fc271b-500.png">
            </a> -->

            <a class="mobile-nav-button" id="mobile-nav-button"></a>

            <ul class="nav" id="nav">
                <li class="nav__item hide-above-md">
                    <a class="nav__link link" href="">Home</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link link "href="mon_espace.php" >Mon espace</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link link " href="carte.php">Map</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link link " href="galerie.php">Galerie</a>
                </li>
               
                <li class="nav__item">
                    <a class="nav__link link " >A propos</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link link "href="logout.php" >Deconnexion </a>
                </li>
                
            </ul>
        </div>



        <div id="transition-wrapper" class="transition-wrapper">
            <div class="container container--full transition-container">

                <a class="section feature" href="http://www.thewildhoneypie.com/videos/tank-and-the-bangas">
                    <div class="feature__content">
                        <h1 class="feature__title header-text">WHERE ARE MY GRAFFS</h1>
                        <p class="feature__subtitle link">Lyon</p>
                    </div>
                    <div class="feature__image">
                        <img class="blob" src="public/img/street.png" alt="logo site">
                    </div>
                </a>

                <div class="section">
                    <a href="http://www.thewildhoneypie.com/videos" class="view-all">View All</a>
                    <a href="/StreetArt/commentaires/minichat.php" class="view-all">Poster</a>
                    <h2 class="header-text header-text--small">Dernieres photos postées</h2>
                    <hr class="line">

                  
                        <div  class="posts">
                        <?php
include('derniers_articles.php');

?>
                            
                        </div>

                        
                    

                </div>

                <hr class="line line--bee-left">
               
                <a class="section feature feature--reverse">
                    <div class="feature__content">
                        <h1 class="feature__title header-text">Partager une oeuvre:</h1>
                        <p class="feature__subtitle link"></p>
                    </div>
                    <div class="feature__image">
                        <img class="blob"
                   src="" alt="">
                    </div>
                </a>
                

                <div class="section">
           
                    <hr class="line">

                    <div class="grid limit-3-4-4">
                       <?php
include("formulaireAjout.php")
                       ?>

                    </div>

                </div>

                <hr class="line line--bee-right">

                <div class="section section__header header-text header-text--medium">
                    <a href="http://www.thewildhoneypie.com/submit"> Sortez de chez vous avant de trouver une raison valable de rester enfermé.</a>
                </div>

               

                <div class="section socials">
                    <hr class="line line--bee-right">
                    <span class="header-text header-text--center">Rejoindre la communauté</span>
                    <div class="socials__icon-wrap">
                        <a class="social-link" href="http://bit.ly/2gPhRIW" target="_blank" rel="nofollow">
                            <img class="social-link__icon" srcset="facebook.png 140w,
                facebook-1.png 240w" sizes="(max-width: 575px) 92px, (max-width: 980px) 116px, 67px" src="facebook-1.png" alt="Facebook">
                        </a>
                        <a class="social-link" href="http://bit.ly/TzrASr" target="_blank" rel="nofollow">
                            <img class="social-link__icon" srcset="youtube.png 140w,
                youtube-1.png 240w" sizes="(max-width: 575px) 92px, (max-width: 980px) 116px, 67px" src="youtube-1.png" alt="YouTube">
                        </a>
                        <a class="social-link" href="http://bit.ly/2zW4IVN" target="_blank" rel="nofollow">
                            <img class="social-link__icon" srcset="instagram.png 140w,
                instagram-1.png 240w" sizes="(max-width: 575px) 92px, (max-width: 980px) 116px, 67px" src="instagram-1.png" alt="Instagram">
                        </a>
                        <a class="social-link" href="http://bit.ly/18kWJ7g" target="_blank" rel="nofollow">
                            <img class="social-link__icon" srcset="twitter.png 140w,
                twitter-1.png 240w" sizes="(max-width: 575px) 92px, (max-width: 980px) 116px, 67px" src="twitter-1.png" alt="Twitter">
                        </a>
                        <a class="social-link" href="http://spoti.fi/2yjqtCz" target="_blank" rel="nofollow">
                            <img class="social-link__icon" srcset="spotify.png 140w,
                spotify-1.png 240w" sizes="(max-width: 575px) 92px, (max-width: 980px) 116px, 67px" src="spotify-1.png" alt="Spotify">
                        </a>
                        <a class="social-link" href="http://www.thewildhoneypie.com/subscribe">
                            <img class="social-link__icon" src="e756ddec0a93e243d8d5106e6e8fc570-291.png" alt="Email">
                        </a>
                    </div>
                </div>

                

        <!-- Footer -->
       
                <p class="footer__copyright paragraph">© 2018 The Wild Vivi. All rights reserved.</p>
           
   

    <!-- Bootstrap core JavaScript -->
    <script></script>
    <script></script>

    <script></script>




</body>

</html>